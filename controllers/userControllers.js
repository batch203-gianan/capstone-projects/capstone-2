const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const Product = require('../models/Product');

//User registration
module.exports.registerUser = (req, res) => {
	//console.log(req.body);

	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: bcrypt.hashSync(req.body.password, 10),
		mobileNumber: req.body.mobileNumber
	})

	console.log(newUser);

	return newUser.save()
	.then(result => {
		console.log(result);
		res.send(true);
		//res.send("User created!");
	})
	.catch(error => {
		console.log(error);
		res.send(false);
		//res.send("User creation failed.");
	})
}

// User authentication
module.exports.loginUser = (req,res) => {
	return User.findOne({email: req.body.email})
	.then(result => {
		if(result == null){
			return res.send(false);
			//return res.send({message: "User does not exist"});
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			if(isPasswordCorrect){
				return res.send({accessToken: auth.creationAccessToken(result)})
			}
			else{
				return res.send(false);
				//return res.send({message: "Incorrect Password"});
			}
		}
	})
}



// Retrieve User Details
module.exports.getProfile = (req,res) => {
	const userData = auth.decode(req.headers.authorization)

	console.log(userData);

	return User.findById(userData.id).then(result => {
		return res.send(result);
	})
}

// View all users (admin)
module.exports.getAllUsers = (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){
		return User.find({})
		.then(result => res.send(result))
	}
	else{
		return res.send({message: "You don't have access to this page!"})
	}
}


// non-admin user checkout

// module.exports.checkout = async (req, res) => {

//     const userData = auth.decode(req.headers.authorization)

//     if(userData.isAdmin){
//         return res.send("Admin account can't create an order!");
//     }
//     else{
//         let isUserUpdated = await User.findById(userData.id).then(user => {
//             user.orders.push({
//                 totalAmount: req.body.totalAmount,
//                 products: req.body.products
//             })

//             //Save the updated user info in the db

//             return user.save()
//             .then(result => {
//                 console.log(result);
//                 return true;
//             })
//             .catch(error => {
//                 console.log(error);
//                 return false;
//             })
//         })
//         let isProductUpdated;
//         for (let i=0; i<req.body.products.length; i++){
//             let data = {
//                 userId: userData.id,
//                 email: userData.email,
//                 productId: req.body.products[i].productId,
//                 productName: req.body.products[i].productName,
//                 quantity: req.body.products[i].quantity
//             }

//              isProductUpdated = await Product.findById(data.productId).then(product => {
//                 product.orders.push({
//                     userId: data.userId,
//                     email: data.email,
//                     quantity: req.body.quantity
//                 })

//                 // Subtraction of stocks

//                 product.stocks -= data.quantity;

//                 return product.save()
//                 .then(result => {
//                     console.log(result);
//                     return true;
//                 })
//                 .catch(error => {
//                     console.log(error);
//                     return false;
//                 })
//             })
//         }   
//         (isUserUpdated && isProductUpdated)? res.send(true) : res.send(false);
//     }
// }

module.exports.checkout = async (data) =>{

	let isUserUpdated = await User.findById(data.userId).then(user =>{

		user.orders.push({productId: data.productId,
			// quantity: data.quantity,
			totalAmount: data.totalAmount
		})


		return user.save().then((ordered, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})

	console.log(isUserUpdated);

	let isProductUpdated = await Product.findById(data.productId).then(product =>{


		product.orders.push({userId: data.userId})


		// product.stocks -= totalAmount;


		return product.save().then((orders, error) =>{
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})

	console.log(isProductUpdated);

	if(isUserUpdated && isProductUpdated){
		return true
	}

	else{
		return false
	}
}








// [STRETCH GOALS] Set user as admin

module.exports.setUserAdmin = (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){
		let isUserAdmin = {
			isAdmin: req.body.isAdmin
		}

		return User.findByIdAndUpdate(req.params.userId, isUserAdmin, {new:true})
		.then(result => {
			console.log(result);
			res.send(result);
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	}
	else{
		res.send("You have no rights to this page!")
	}
}

