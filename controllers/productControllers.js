const Product = require('../models/Product');
const auth = require('../auth');

// Add a Product (admin only)
module.exports.addProduct = (req,res) => {
	let userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		let newProduct = new Product({
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			stocks: req.body.stocks
		})

		return newProduct.save()
		.then(result => res.send(true))
		//.then(result => res.send("Product Added!"))
		.catch(error => res.send(false))
		//.catch(error => res.send(error.message))
	}
	else{
		res.send(false);
		//res.send({message: "You are not authorized to add a Product!"});
	}
}

// Retrieve all Products (admin, user and guest)

module.exports.getAllActiveProducts = (req,res) => {
	return Product.find({isActive: true})
	.then(result => res.send(result));
}

// Retrieve a single product
module.exports.getSingleProduct = (req, res) => {
	console.log(req.params.productId);
	return Product.findById(req.params.productId)
	.then(result => res.send(result));
}

// Update a product (admin)
module.exports.updateProduct = (req,res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin){
		let updateProduct = {
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			stocks: req.body.stocks
		}

		return Product.findByIdAndUpdate(req.params.productId, updateProduct, {new:true})
		.then(result => {
			console.log(result);
			res.send(result)
		})
		.catch(error => {
			console.log(error);
			res.send(false)
			//res.send("You are not authorized")
		})
	}
	else {
		return res.send("you dont have access to this page!")
	}

}

//Archiving a product (admin only)

module.exports.archiveProduct = (req,res) => {
	const userData = auth.decode(req.headers.authorization);

	let updateIsActiveProduct = {
		isActive: req.body.isActive
	}

	if(userData.isAdmin){
		return Product.findByIdAndUpdate(req.params.productId, updateIsActiveProduct)
		.then(result => {
			console.log(result);
			res.send(true)
			//res.send("Product Archived")
		})
		.catch(error => {
			console.log(error);
			res.send(false)
			//res.send("Archive Failed")
		})
	}
	else{
		return res.send(false)
		//return res.send({message: "You don't have access to this page!"})
	}
}

// Retrieve all products (admin only)
module.exports.getAllProducts = (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){
		return Product.find({})
		.then(result => res.send(result))
	}
	else{
		return res.send(false)
		//return res.send({message: "You don't have access to this page!"})
	}
}