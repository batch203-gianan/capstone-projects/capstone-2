const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Product name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	// stocks: {
	// 	type: Number,
	// 	required: [true, "stocks is required"]
	// },
	price:{
		type: Number,
		required: [true, "price is required"]
	},
	isActive:{
		type: Boolean,
		default: true
	},
	createdOn:{
		type: Date,
		default: new Date()
	},
	orders: [{

		orderId: {
			type: String
		},
		userId:{
			type: String
		},
		email: {
			type: String
		},
		quantity:{
			type: Number
		},
		purchaseOn:{
			type: Date,
			default: new Date()
			
		}
	}]
})

module.exports = mongoose.model("Product", productSchema);