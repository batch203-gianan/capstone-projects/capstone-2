const jwt = require('jsonwebtoken');
const secret = "capstone2API";

// Token Creation
module.exports.creationAccessToken = (user) => {
	console.log(user);

	const data = {
		id: user._id,
		email: user.email,
		isAdmin : user.isAdmin
	}

	return jwt.sign(data, secret, {});
}

// Token Verification
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;

	if(token !== undefined){
		token = token.slice(7, token.length);
		console.log(token);

		return jwt.verify(token, secret, (error, data) => {

			if(error){
				return res.send({authorization: "Invalid Token"})
			}

			else{
				next();
			}
		})
	}
	else{
		res.send({message: "Authorization failed! No token provided!"})
	}
}

// Token decryption

module.exports.decode = (token) => {
	if(token !== undefined){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data)=> {
			if(err){
				return null;
			}
			else{
				return jwt.decode(token, {complete: true}).payload
			}
		})
	}
	else{
		return null
	}
}