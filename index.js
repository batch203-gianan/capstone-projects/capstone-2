const express = require('express');
const mongoose = require('mongoose');
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const cors = require('cors');


// Server creation
const app = express();

// MongoDB Connection
mongoose.connect("mongodb+srv://scottgianan:sharicekaie12@wdc028-course-booking.jxpj05l.mongodb.net/Capstone-2-API?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database."));

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

// User Route
app.use('/users', userRoutes);

// Product Route
app.use('/products', productRoutes)


// Port & Listen
const port = process.env.PORT || 4000
app.listen(port, ()=> {
	console.log(`API is now online on port ${port}`);
});