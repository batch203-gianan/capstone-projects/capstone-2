const express = require('express');
const router = express.Router();
const auth = require('../auth');

const userControllers = require('../controllers/userControllers');

console.log(userControllers);

//Route for user registration
router.post('/register', userControllers.registerUser);

//Route for viewing all users (admin)
router.get("/allUsers", auth.verify, userControllers.getAllUsers);

// Route for setting a specific user as Admin
router.put('/setAsAdmin/:userId', auth.verify, userControllers.setUserAdmin)

//Route for user auth
router.post("/login", userControllers.loginUser);

//Route for viewing user profile
router.get("/profile",auth.verify, userControllers.getProfile);

//Route for user checkout
//router.post("/checkout", auth.verify, userControllers.checkout);
router.post("/checkout", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization);

	let data = {
		userId: userData.id,
		productId: req.body.productId,
		// quantity: req.body.quantity,
		totalAmount: req.body.totalAmount
	}

	if(userData.isAdmin){
		res.send("You're not allowed to access this page!")
	}
	else{
	userControllers.checkout(data).then(resultFromController => res.send(resultFromController));
	}
})


module.exports = router;