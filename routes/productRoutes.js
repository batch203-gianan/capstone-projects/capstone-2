const express = require('express');
const router = express.Router();
const productControllers = require('../controllers/productControllers');
const auth = require('../auth');

console.log(productControllers);

//Route for adding a product (admin only)
router.post("/", auth.verify, productControllers.addProduct);

//Route for getting all products (admin)
router.get('/all', auth.verify, productControllers.getAllProducts);

// Route for getting all active products (admin, user and guest)
router.get('/', productControllers.getAllActiveProducts);

// Route for getting a single product
router.get('/:productId', productControllers.getSingleProduct);

//Route for updating a product (admin)
router.put("/:productId", auth.verify, productControllers.updateProduct);

//Route for arhiving a product (admin)
router.patch('/archive/:productId', auth.verify, productControllers.archiveProduct);





module.exports = router;